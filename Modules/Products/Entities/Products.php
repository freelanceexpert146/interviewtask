<?php

namespace Modules\Products\Entities;

use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    use Sluggable,SluggableScopeHelpers;
    
    protected $table = 'products';
    
    protected $fillable = [ 'slug','user_id','name','upc','price','image','status'];
    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name',
                'onUpdate'=>false
            ]
        ];
    }

     //Getting picture path
    public function getPicturePathAttribute()
    {
        return ($this->image) ? \URL::to('storage/app/public/products/'.$this->image) : NULL;
    }
}
