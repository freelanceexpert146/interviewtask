<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['web','auth']], function () {
    Route::resource('products', 'ProductsController');
    Route::delete('products-deleteall', 'ProductsController@deleteAll')->name('products.myproductsDeleteAll');
    Route::post('products/media/upload', 'ProductsController@saveMedia')->name('products.mediaStore');
    Route::get('products/status/{slug}', 'ProductsController@status')->name('products.status');
});
