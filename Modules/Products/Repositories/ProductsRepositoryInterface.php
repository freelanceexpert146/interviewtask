<?php

namespace Modules\Products\Repositories;


interface ProductsRepositoryInterface
{
    public function getMyProjects($request);

    public function getRecord($id);
    
    public function getRecordBySlug($slug);

    public function store($request);

    public function update($request,$id);

    public function destroy($id);
    
    public function saveProductsPictureMedia($request);

    public function deleteMultipleRecords($request);
}