<?php

namespace Modules\Products\Repositories;

use Modules\Products\Entities\Products;
use DB,Mail,Session;
use DataTables;
use Illuminate\Support\Facades\Input;
use Log;

class ProductsRepository implements ProductsRepositoryInterface {

    protected $model = 'Products';

    function __construct(Products $Products) {
        $this->Products = $Products;
    }

    public function getMyProjects($request)
    {
      return $this->Products->where('user_id',auth()->user()->id)->get();
    }

    public function getRecord($id)
    {
      return $this->Products->find($id);
    }

    public function getRecordBySlug($slug)
    {
      return $this->Products->findBySlug($slug);
    }

    public function store($request)
    {
        try {
            $filleable = $request->only('slug','name','upc','price','status');
            if ($request->get('image')) $filleable['image'] = $request->get('image');
            $filleable['user_id'] = auth()->user()->id;
            $this->Products->fill($filleable);
            $this->Products->save();
            $response['message'] = trans('flash.success.products_created_successfully');
            $response['type'] = 'success';
            $response['reset'] = 'true';
            $response['status_code'] = 200;
            $response['url'] = route('products.index');
        }catch (Exception $ex) {
            $response['message'] = trans('flash.error.oops_something_went_wrong_creating_record');
            $response['type'] = 'error';
        }   
         return $response;  
    }

    public function update($request,$id)
    {
        try {
            $filleable = $request->only( 'slug','name','upc','price','status');
            if ($request->get('image')) $filleable['image'] = $request->get('image');
            $record = $this->getRecord($id);
            $record->fill($filleable);
            $record->save();
            $response['message'] = trans('flash.success.products_updated_successfully');
            $response['type'] = 'success';
            $response['reset'] = 'true';
            $response['status_code'] = 200;
            $response['url'] = route('products.index');
        }catch (Exception $ex) {
            $response['message'] = trans('flash.error.oops_something_went_wrong_updating_record');
            $response['type'] = 'error';
        } 
        return $response;
    }

    public function destroy($id)
    {
       $record = $this->Products->find($id);
       if($record){
            return $this->Products->destroy($id);
       }
       return false;
    }

    public function saveProductsPictureMedia($request)
    {
         $filename = uploadWithResize($request->file('files'), storage_path() . '/app/public/products/');
         $response['status'] = true;
         $response['filename'] = $filename;
         return $response;
    } 

    public function deleteMultipleRecords($request)
    {
        $ids = $request->ids;
        $this->Products->whereIn('id',explode(",",$ids))->delete();
        $response['status'] = true;
        $response['success'] = 'Products Deleted successfully.';
        return $response;
    }
}
