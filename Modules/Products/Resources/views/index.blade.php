@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        @if($records->count()>0)
         <button style="margin-bottom: 10px" class="btn btn-primary delete_all" data-url="{{ route('products.myproductsDeleteAll') }}">
            Delete All Selected</button>
        @endif
        <h2>My Products</h2>&nbsp;&nbsp;&nbsp;
        <a href="{{route('products.create')}}" class="btn btn-primary"> Add Product</a>
        <table class="table" style="margin-top: 10px;">
          <thead>
            <tr>
              <th scope="col" width="50px"><input type="checkbox" id="master"></th>
              <th scope="col">Name</th>
              <th scope="col">Price</th>
              <th scope="col">UPC</th>
              <th scope="col">Image</th>
              <th scope="col">Status</th>
              <th scope="col">Action</th>
            </tr>
          </thead>
          <tbody>
            @if($records->count()>0)
                @foreach($records as $product)
                    <tr>
                      <td><input type="checkbox" class="sub_chk" data-id="{{$product->id}}"></td>
                      
                      <td>{{ucfirst($product->name)}}</td>
                      <td>{{$product->price}}</td>
                      <td> {!! DNS2D::getBarcodeHTML($product->upc, 'QRCODE') !!}</td>
                      <td><img src="{{$product->picture_path}}" height="200" width="200"></td>
                      <td>{{ucfirst($product->status)}}</td>
                      <td>
                        <a href="{{route('products.edit',$product->slug)}}">Edit</a>
                        <form method="POST" action="{{route('products.destroy',$product->slug)}}" accept-charset="UTF-8" style="display:inline" class="dele_{{$product->id}}">
                        <input name="_method" value="DELETE" type="hidden">
                        @csrf
                            <span>
                                 &nbsp;<a href="javascript:;" id="dele_{{$product->id}}" data-toggle="tooltip" title="Delete" type="button"  data-placement="top" name="Delete" class="delete_action tble_button_st tooltips" Onclick="return ConfirmDeleteLovi(this.id,this.name,this.name);" ><i class="fa fa-trash-o" title="Delete"></i>
                                </a>
                             </span>
                        </form>
                      </td>
                    </tr>
                @endforeach
            @else
                <tr>
                  <th class="text-center" colspan="6">No Products Available</th>
                </tr>
            @endif
          </tbody>
        </table>
    </div>
</div>
@endsection

@section('uniquePageScript')
<script>
     $(document).ready(function () {
        $('#master').on('click', function(e) {
         if($(this).is(':checked',true))  
         {
            $(".sub_chk").prop('checked', true);  
         } else {  
            $(".sub_chk").prop('checked',false);  
         }  
        });

        $('.delete_all').on('click', function(e) {
            var allVals = [];  
            $(".sub_chk:checked").each(function() {  
                allVals.push($(this).attr('data-id'));
            });  

            if(allVals.length <=0)  
            {  
                alert("Please select row.");  
            }  else {  
                var check = confirm("Are you sure you want to delete this row?");  
                if(check == true){  
                    var join_selected_values = allVals.join(","); 
                    $.ajax({
                        url: $(this).data('url'),
                        type: 'DELETE',
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        data: 'ids='+join_selected_values,
                        success: function (data) {
                            if (data['success']) {
                                $(".sub_chk:checked").each(function() {  
                                    $(this).parents("tr").remove();
                                });
                                alert(data['success']);
                            } else if (data['error']) {
                                alert(data['error']);
                            } else {
                                alert('Whoops Something went wrong!!');
                            }
                        },
                        error: function (data) {
                            alert(data.responseText);
                        }
                    });

                  $.each(allVals, function( index, value ) {
                      $('table tr').filter("[data-row-id='" + value + "']").remove();
                  });

                }  
            }  
        });

        

        $(document).on('confirm', function (e) {

            var ele = e.target;

            e.preventDefault();


            $.ajax({

                url: ele.href,

                type: 'DELETE',

                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},

                success: function (data) {

                    if (data['success']) {

                        $("#" + data['tr']).slideUp("slow");

                        alert(data['success']);

                    } else if (data['error']) {

                        alert(data['error']);

                    } else {

                        alert('Whoops Something went wrong!!');

                    }

                },

                error: function (data) {

                    alert(data.responseText);

                }

            });


            return false;

        });

    });
</script>
@endsection
