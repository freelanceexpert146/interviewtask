@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Add Product') }}</div>

                <div class="card-body">
                     {!! Form::open(['route' => 'products.store','class'=>'form-horizontal','id'=>'F_AddProducts']) !!}
                      @include('products::form')
                      <div class="form-group row mb-0">
                          <div class="col-md-6 offset-md-4">
                              <button type="submit" class="btn btn-primary directSubmit" id="AddProducts">
                                  {{ __('Save') }}
                              </button>
                               <button type="reset" class="btn btn-default">Reset</button>
                          </div>
                      </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
